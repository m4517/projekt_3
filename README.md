# MUS2 Projekt
## Aufgabe 3 – Transfer learning

Um die Vorteile von Transfer learning zu demonstrieren wurde ein Classifier basierend auf dem CIFAR-10 Datensatz entwickelt. Als Programmiersprache wurde Python gewählt, da diese bei machine-learning Anwendungen Großteils zum Einsatz kommt.
Zunächst wurde der Datensatz geladen. Dieser besteht aus 60000 Bildern von 10 verschiedenen Objekten, 10000 Trainings- und 50000 Testbilder. Für das Projekt sollten einmal die Objekte Hund und Katze, sowie Pferd und Wild trainiert werden. Dabei sollte gezeigt werden, dass die Gewichtungen, die beim Lernen von zwei verschiedenen Klassen erzeugt werden weiter verwendet werden können um das Lernen auf andere Klassen zu beschleunigen. 
Aus dem Datensatz wurden zunächst die gewünschten Bilder für Hund, Katze, Wild und Pferd extrahiert. Anschließend wurden die Bilder Normalisiert. Dabei werden die Bildwerte die sich von 0 bis 255 erstrecken skaliert damit sie sich anschließend zwischen null und eins erstrecken. Anschließend wurde das CNN erstellt. Es wurden verschiedenen Convolutional Layers gefolgt von Pooling Layers erstellt. Anschließend wurde noch ein Dense Layer erstellt. 
Zunächst wurde das Model 50 Epochen lang auf Pferd und Wild trainiert. Die Genauigkeit auf den Testdatensatz betrug dabei 87%. Anschließend wurde ein neues Modell auf Hund und Katze gelernt, wobei eine Genauigkeit von 71% erzielt werden konnte. Anschließend wurde ein neues Modell 50 Epochen lang auf Hund und Katze trainiert, allerdings die Gewichtungen des Trainingsmodells, welches zuvor auf Pferd und Wild trainiert wurde initial übergeben. 
Wie man anhand der Abbildungen erkennen kann, benötigt der bereits vorgelernte Datensatz wesentlich weniger Iterationen um eine ähnliche Genauigkeit zu erzielen, wie es beim ungelernten Datensatz der Fall ist. Nach vielen Iterationen ergibt gleichen sich die beiden Kurven an. Es konnte also gezeigt werden, dass das wiederverwenden bereits gelernter Gewichtungen bei ähnlichen Problemen durchaus den Lernprozess positiv beeinflussen kann. Vor allem bei größeren Datensätzen und komplexeren Problemen wo teilweise Hochleistungsrechner am Werk sind, kann damit viel Rechenzeit gespart werden und somit auch die Kosten minimiert werden. 

<img src="https://gitlab.com/m4517/projekt_3/-/raw/main/Doku/ABB_1.png" width=50% height=50%>


<img src="https://gitlab.com/m4517/projekt_3/-/raw/main/Doku/ABB_2.png" width=50% height=50%>


<img src="https://gitlab.com/m4517/projekt_3/-/raw/main/Doku/ABB_3.png" width=50% height=50%>


https://machinelearningmastery.com/how-to-develop-a-cnn-from-scratch-for-cifar-10-photo-classification/