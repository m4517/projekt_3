import os
import sys
import time as time
#from traceback import print_list
#from keras.utils.data_utils import get_file
#from keras.utils.tf_inspect import getfile
import numpy as np
from matplotlib import pyplot
from keras.datasets import cifar10
from keras.utils.np_utils import to_categorical
from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Dense
from keras.layers import Flatten
from tensorflow.keras.optimizers import SGD

np.set_printoptions(threshold=sys.maxsize)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

def load_dataset(a,b):
	# load train and test dataset
	(train_images, train_labels), (test_images, test_labels) = cifar10.load_data()

	# Train
	idx = (train_labels == a).reshape(train_images.shape[0])
	TrainIA = train_images[idx]
	#print('TrainIA: {}'.format(TrainIA.shape))
	idx = (train_labels == a).reshape(train_labels.shape[0])
	TrainLA = train_labels[idx]
	#print('TrainLA: {}'.format(TrainLA.shape))
	idx = (train_labels == b).reshape(train_images.shape[0])
	TrainIB = train_images[idx]
	#print('TrainIB: {}'.format(TrainIB.shape))
	idx = (train_labels == b).reshape(train_labels.shape[0])
	TrainLB = train_labels[idx]
	#print('TrainLB: {}'.format(TrainLB.shape))

	# Test 
	idx = (test_labels == a).reshape(test_images.shape[0])
	TestIA = test_images[idx]
	#print('TestIA: {}'.format(TestIA.shape))
	idx = (test_labels == a).reshape(test_labels.shape[0])
	TestLA = test_labels[idx]
	#print('TestLA: {}'.format(TestLA.shape))
	idx = (test_labels == b).reshape(test_images.shape[0])
	TestIB = test_images[idx]
	#print('TestIB: {}'.format(TestIB.shape))
	idx = (test_labels == b).reshape(test_labels.shape[0])
	TestLB = test_labels[idx]
	#print('TestLB: {}'.format(TestLB.shape))

	arrTrainX = np.concatenate((TrainIA,TrainIB))
	arrTrainY = np.concatenate((TrainLA,TrainLB))
	arrTestX = np.concatenate((TestIA,TestIB))
	arrTestY = np.concatenate((TestLA,TestLB))

	arrTrainY[arrTrainY == a] = 0
	arrTrainY[arrTrainY == b] = 1
	arrTestY[arrTestY == a] = 0
	arrTestY[arrTestY == b] = 1 

	arrTrainY = to_categorical(arrTrainY,2)
	arrTestY = to_categorical(arrTestY,2)

	return arrTrainX, arrTrainY, arrTestX, arrTestY

# scale pixels
def prep_pixels(train, test):
	# convert from integers to floats
	train_norm = train.astype('float32')
	test_norm = test.astype('float32')
	# normalize to range 0-1
	train_norm = train_norm / 255.0
	test_norm = test_norm / 255.0
	# return normalized images
	return train_norm, test_norm

# define cnn model
def define_model():
	model = Sequential()
	model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=(32, 32, 3)))
	model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
	model.add(MaxPooling2D((2, 2)))
	model.add(Flatten())
	model.add(Dense(128, activation='relu', kernel_initializer='he_uniform'))
	model.add(Dense(2, activation='softmax'))
	# compile model
	opt = SGD(learning_rate=0.0025, momentum=0.9)
	model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])
	return model

# plot diagnostic learning curves
def summarize_diagnostics(history,name):
	pyplot.subplot(111)
	pyplot.title('Classification Accuracy')
	pyplot.plot(history.history['accuracy'], color='blue', label='train')
	pyplot.plot(history.history['val_accuracy'], color='orange', label='test')
	# save plot to file
	pyplot.savefig('MUS_'+ name + '.png')
	pyplot.close()

def Run_MUS2():
	start = time.time()
	#load Dtasets
	print("load Dataset")
	trainX, trainY, testX, testY = load_dataset(3,5)
	trainX2,trainY2,testX2,testY2= load_dataset(4,7)

	#prepare Pixel Data
	print("prepare pixel data")
	trainX, testX = prep_pixels(trainX, testX)
	trainX2, testX2 = prep_pixels(trainX2, testX2)

	# define model
	print("Define Model CAT DOG")
	modelCD = define_model()

	print("Define Model Deer Horse")
	modelDH = define_model()

	print("Define Model CAT DOG NEW")
	modelCD2 = define_model()

	print("fit model CAT DOG")
	historyCD = modelCD.fit(trainX, trainY, epochs=30, batch_size=64, validation_data=(testX, testY), verbose=0)

	# evaluate model
	print("evaluate model CAT DOG")
	_, acc = modelCD.evaluate(testX, testY, verbose=0)
	print('> %.3f' % (acc * 100.0))

	print("fit model DEER HORSE")
	historyDH = modelDH.fit(trainX2, trainY2, epochs=30, batch_size=64, validation_data=(testX2, testY2), verbose=0)

	# evaluate model
	print("evaluate model DEER HORSE")
	_, acc = modelDH.evaluate(testX2, testY2, verbose=0)
	print('> %.3f' % (acc * 100.0))

	#Tranfer Weights
	weights = modelDH.get_weights()
	modelCD2.set_weights(weights)

	# evaluate model
	print("fit model CAT DOG with Weights")
	historyCD2 = modelDH.fit(trainX, trainY, epochs=30, batch_size=64, validation_data=(testX, testY), verbose=0)

	# evaluate model
	print("evaluate model CAT DOG with weights")
	_, acc = modelCD2.evaluate(testX, testY, verbose=0)
	print('> %.3f' % (acc * 100.0))

	# learning curves
	print("learning curves")
	summarize_diagnostics(historyCD,'CD')
	summarize_diagnostics(historyDH,'DH')
	summarize_diagnostics(historyCD2,'CD-2')
	ende = time.time()
	print('Gesamtzeit: {:5.0f}min'.format((ende-start)/60))

# entry point, run the test harness
Run_MUS2()